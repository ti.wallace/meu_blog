<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\PostagemCollection;
use App\Http\Resources\Postagem as PostagemResource;
use App\Postagem;

class PostagemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * PostagemResource
     */
    public function index()
    {
        return new PostagemCollection(Postagem::paginate(3));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return PostagemResource
     */
    public function store(Request $request)
    {
        $postagem = Postagem::gravar($request->titulo, $request->postagem, $request->autor);

        return new PostagemResource($postagem);

    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return PostagemResource
     */
    public function show($id)
    {
        return new PostagemResource(Postagem::FindOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return PostagemResource
     */
    public function update(Request $request, $id)
    {
        $postagem = Postagem::atualizar($id, $request->titulo, $request->postagem, $request->autor);

        return new PostagemResource($postagem);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return PostagemResource
     */
    public function destroy($id)
    {
        $postagem = Postagem::deletar($id);

        return new PostagemResource($postagem);

    }

}
