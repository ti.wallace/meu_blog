<?php

namespace App\Http\Controllers;

use App\Postagem;
use Illuminate\Http\Request;

class PostagemController extends Controller
{
    /**
     * Listagem das Postagens.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('blogDashBoard');
    }

    /**
     * Cadastro da Postagem.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addPostagem()
    {
        return view('add-postagem');
    }

}
