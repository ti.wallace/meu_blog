<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postagem extends Model
{
    /**
     * Tabela no banco de dados.
     *
     * @var string
     */
    protected $table = 'postagens';

    /**
     * Grava uma nova Postagem.
     *
     * @param $titulo
     * @param $postagem
     * @param $autor
     * @return Postagem
     */
    public static function gravar($titulo, $postagem, $autor)
    {
        $postagem = new self();

        $postagem->titulo = $titulo;
        $postagem->postagem = $postagem;
        $postagem->titulo = $autor;

        $postagem->save();

        return $postagem;

    }

    /**
     * Atualiza uma Postagem.
     *
     * @param $id
     * @param $titulo
     * @param $postagem
     * @param $autor
     * @return mixed
     */
    public static function atualizar($id, $titulo, $postagem, $autor)
    {
        $postagem = self::findOrFail($id);

        $postagem->titulo = $titulo;
        $postagem->postagem = $postagem;
        $postagem->titulo = $autor;

        $postagem->save();

        return $postagem;

    }

    /**
     * Deleta uma Postagem.
     *
     * @param $id
     * @return mixed
     */
    public static function deletar($id)
    {
        $postagem = self::findOrFail($id);

        $postagem->delete();

        return $postagem;

    }

}
