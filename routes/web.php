<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# Postagens
Route::get('blogDashboard','PostagemController@index')->name('blog-dashboard');
Route::get('add-postagens','PostagemController@addPostagem')->name('add-postagens');

# Auth
Auth::routes();

Route::get('/home', 'PostagemController@index')->name('home');
