@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="postAdd col-lg-12 text-right" style="margin-bottom: 20px;">
                <a href="{{url('/add-postagens')}}" class="btn btn-primary">Add Postagem</a>
            </div>
        </div>
        <postagens-component></postagens-component>

    </div>
@endsection
