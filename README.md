<h1 align="center">Desafio Benfeitoria</h1>

<h2 align="center"><img src="https://gitlab.com/ti.wallace/meu_blog/raw/master/demo.png" alt="Meu Blog"></img></h2>

## Linguagem utilizada

- Php (v7.2)

## Tecnologias utilizadas

- Laravel (v6)
- Vue JS
- Mysql

## Recursos do Laravel

- Migrations
- Seed Factory
- Resources Api

## Controle de versionamento.

- Git (Git Flow)

## Como instalar a aplicação.

- Faça o clone do projeto para o seu repositorio local.

 ``` bash
$ git clone https://gitlab.com/ti.wallace/meu_blog.git
```

- Navegue ate a raiz do diretório do projeto.

``` bash
$ cd patch/to/project/meu_projeto
```

- Instale as depenências utilizando o Composer.

``` bash
$ composer install
```

- Gere a chave da aplicação:

``` bash
$ php artisan key:generate
```

- Configure o seu arquivo .env:

``` bash
$ vim .env
```

- Execute a migração para a criação das tabelas no seu banco de dados:

``` bash
$ php artisan migrate
```

- Caso deseje, popule a tabela de postagens:

``` bash
$ php artisan db:seed
```
## Mapa de acessos Rest

| Method    | URI                      | Name              | Action                                                                 | Middleware   |
|-----------|--------------------------|-------------------|------------------------------------------------------------------------|--------------|
| GET       | api/postagens            | postagens.index   | App\Http\Controllers\Api\PostagemController@index                      | api          |
| POST      | api/postagens            | postagens.store   | App\Http\Controllers\Api\PostagemController@store                      | api          |
| GET       | api/postagens/{id}       | postagens.show    | App\Http\Controllers\Api\PostagemController@show                       | api          |
| PUT       | api/postagens/{id}       | postagens.update  | App\Http\Controllers\Api\PostagemController@update                     | api          |
| DELETE    | api/postagens/{id}       | postagens.destroy | App\Http\Controllers\Api\PostagemController@destroy                    | api          |

