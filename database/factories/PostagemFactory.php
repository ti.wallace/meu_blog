<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Postagem;
use Faker\Generator as Faker;

$factory->define(Postagem::class, function (Faker $faker) {
    return [
        'titulo' => $faker->sentence($nbWords = 10),
        'postagem' => $faker->paragraph($nbSentences = 15),
        'autor' => $faker->name
    ];
});
